package com.example.examen;
import androidx.appcompat.app.AppCompatActivity;

import java.io.Serializable;

public class RectanguloActivity implements Serializable {

    private float base;
    private float altura;

    public RectanguloActivity(float base, float altura) {
        this.base = base;
        this.altura = altura;
    }

    public RectanguloActivity() {
    }

    public float getBase() {
        return base;
    }

    public void setBase(float base) {
        this.base = base;
    }

    public float getAltura() {
        return altura;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }

    public float calcularArea(){
        float area = this.base*this.altura;
        return area;
    }
    public float calcularPerimetro(){
        float perimetro = (this.altura*2)+(this.base*2);
        return perimetro;
    }
}
